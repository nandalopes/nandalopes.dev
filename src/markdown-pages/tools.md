---
slug: "/dev-tools"
date: "2019-05-04"
title: "Ferramentas para desenvolvedores"
---

## Ferramentas para devs

Meu repositório de dotfiles, onde fico escovando bits...
- https://github.com/nandalopes/dotfiles-yadr

Lipsum tá diferente...
- https://slipsum.com/

Um só gerenciador de versões de linguagens, tipo Python, Ruby, NodeJS, etc +
direnv
pra só carregar o necessário e quando for necessário
- https://github.com/asdf-community/asdf-direnv

Clonando repositórios gigantes:
- https://www.atlassian.com/git/tutorials/big-repositories
- https://github.blog/2020-01-17-bring-your-monorepo-down-to-size-with-sparse-checkout/#cloning-in-sparse-mode

`myrepos` pra fazer atualização dos repositórios todos num só comando 😃
- https://myrepos.branchable.com/

Usos avançados de HEREDOCS no shell
- http://xpt.sourceforge.net/techdocs/nix/shell/gsh09-SyntaxAndUsages/ar01s08.html

Fuzzy finder, bem rápido
- https://github.com/junegunn/fzf/wiki/Configuring-fuzzy-completion

Terminal leve com suporte a modo Quake, amo
- https://gnunn1.github.io/tilix-web/manual/quake/

Descrição de setup de hardware e software do Fábio Akita
- https://www.youtube.com/watch?v=epiyExCyb2s

Sobre redes sociais e estatísticas:
- https://www.akitaonrails.com/2020/09/30/akitando-84-entendendo-o-dilema-social-e-como-voce-e-manipulado
